var Timer = require('../index.js');

var timer = new Timer();

timer.on('start',function(){
    console.log('>>','TIMER STARTED');
});
timer.on('pause', function () {
    console.log('>>', 'TIMER PAUSED');
});
timer.on('unpause', function () {
    console.log('>>', 'TIMER UNPAUSED');
});
timer.on('stop', function () {
    console.log('>>', 'TIMER STOPPED');
    console.log('---------------------------------');
});

timer.on(1,function(){
    console.log('>>',1);
});
timer.on(2, function () {
    console.log('>>', 2);
});
timer.on(3, function () {
    console.log('>>', 3);
    timer.pause();
    pauseTimer.start();
});
timer.on(4, function () {
    console.log('>>', 4);
});
timer.on(5, function () {
    console.log('>>', 5);
    timer.stop();
});

var pauseTimer = new Timer();

pauseTimer.on(1,function(){
    console.log('PAUSED >>',1);
});
pauseTimer.on(2, function () {
    console.log('PAUSED >>', 2);
});
pauseTimer.on(3, function () {
    console.log('PAUSED >>', 3);
    pauseTimer.stop();
    timer.unpause();
});

timer.start();