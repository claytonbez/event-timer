var EventEmitter = require('events');

class Timer extends EventEmitter {
    constructor(opts) {
        super();
        this._emit = this.emit;
        this._on = this.on;
        this.ops;

        if(opts === undefined){
            this.opts = { interval:1000 };
        }
        else{
            this.opts = opts;
        }

        this.time = 0;
        this.p = 0;
        this.timer;
    }
    set(time) {
        this.time = time;
    }
    zero() {
        this.time = 0;
        this._emit(0);
    }
    start() {
        this._emit('start');
        this.timer = setInterval(() => {
            this._doTimerFunction();
        }, this.opts.interval);
    }
    _doTimerFunction() {
        if (!this.p) {
            this.time++;
            this._emit(this.time);
        }
    }
    pause() {
        this.p = 1;
        this.emit('pause');
        return;
    }
    unpause() {
        this.p = 0;
        this.emit('unpause');
        return;
    }
    stop() {
        clearInterval(this.timer);
        this._emit('stop');
        return;
    }
    getTime() {
        return this.time;
    }

}

module.exports = Timer;