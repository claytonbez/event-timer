## event-timer

An event based timer class.

## Install

#### npm:

` npm install node-event-timer --save `

## Usage

```
    var Timer = require('node-event-timer);

    var timer = new Timer();

    timer.on('start',function(){
        console.log('>>', 'TIMER STARTED');
    });

    timer.on(1,function(){
        console.log('>>', 1);
    });

    timer.on(2, function () {
        console.log('>>', 2);
    });

    timer.on(3, function () {
        console.log('>>', 3);
        timer.stop();
    });

    timer.on('stop',function(){
        console.log('>>', 'TIMER STOPPED');
    });
```